# CPU Usage Service

Service which collects data regarding CPU usage of the clients  

## Design reasoning
The service can receive events by HTTP requests, get events for specified client or all clients.  
It uses redis as a storage of events. The reason for that is an assumption that there are a lot of clients   
sending events in high frequency and it's needed to determine clients with high CPU usage in (near) real-time.  
Otherwise, it's also possible to use some NoSQL storage like MongoDB.

## Prerequisites

Installed [sbt](https://www.scala-sbt.org/download.html)  
Running [redis](https://redis.io) locally or export environment variable e.g.:
```shell
export REDIS_URL="redis://localhost:6379"
```

## Running the application

```sbtshell
sbt "runMain com.hivestreaming.Server"
```

## Build

Application could be build using [SBT Native Packager](https://www.scala-sbt.org/sbt-native-packager/)

For example:
```sbtshell
sbt universal:packageBin
```

To run application from build:

Extract zip
```
unzip target/universal/cpu-usage-service-0.1.zip -d target/universal/
```

Run it
```
./target/universal/cpu-usage-service-0.1/bin/cpu-usage-service
```

Using SBT Native packager, it's also possible to create a docker image

## Example

Sending new event:  
Request:  
```bash
curl -X POST \
  http://localhost:8080/metrics/client-id-1 \
  -H 'Content-Type: application/json' \
  -d '{"cpu": 99.8,"time": "2018-09-24T17:01:12.925+0000"}'
```
Response:  
```bash
[{"cpu":99.8,"time":"2018-09-24T17:01:12.925+0000"}]
```

Receiving event for client  
Request:  
```shell
curl -X GET http://localhost:8080/metrics/client-id-1
```
Resposne:  
```shell
[{"cpu":99.8,"time":"2018-09-24T17:01:12.925+0000"}]
```

## Possible improvements

- Add storage for historical events and later analytics e.g. MongoDB
- Add authentication between client and server
- Add filtering and aggregation of events to API e.g. clients with min CPU load from specified date
- Add analytics which will automatically determine clients with problems
- Add swagger API
- Improve logging
- Improve test coverage