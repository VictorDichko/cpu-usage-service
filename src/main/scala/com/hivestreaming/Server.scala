package com.hivestreaming

import cats.effect._
import cats.implicits._
import com.hivestreaming.models.Metrics
import com.hivestreaming.store.{ClientsStore, MetricsStore}
import com.typesafe.config.ConfigFactory
import org.http4s.HttpRoutes
import org.http4s.server.blaze.BlazeBuilder
import redis.clients.jedis.JedisPool
import scalacache.Cache
import scalacache.redis.RedisCache
import scalacache.serialization.binary._

import scala.language.higherKinds

object Server extends IOApp {
  def run(args: List[String]): IO[ExitCode] = {

    ServerStream.stream[IO].compile.drain.as(ExitCode.Success)
  }
}

object ServerStream {


  lazy val redisUrl: String = ConfigFactory.defaultApplication().resolve().getString("redisUrl")
  lazy val jedisPool = new JedisPool(redisUrl)

  lazy val clientsRedis: Cache[List[String]] = RedisCache(jedisPool)
  lazy val metricsRedis: Cache[List[Metrics]] = RedisCache(jedisPool)

  def clientsStore[F[_] : Async] = new ClientsStore[F](clientsRedis)
  def metricsStore[F[_] : Async] = new MetricsStore[F](metricsRedis)

  def service[F[_] : Async]: Service[F] = new Service[F](clientsStore, metricsStore)

  def routes[F[_] : Effect]: HttpRoutes[F] = new Routes[F](service).routes

  def stream[F[_] : ConcurrentEffect]: fs2.Stream[F, ExitCode] =
    BlazeBuilder[F]
      .bindHttp(8080, "0.0.0.0")
      .mountService(routes, "/")
      .serve
}
