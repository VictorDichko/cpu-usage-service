package com.hivestreaming

import java.time.ZonedDateTime

import cats.effect.Async
import cats.implicits._
import com.hivestreaming.models.{Formats, Metrics}
import io.circe.Json
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.{EntityDecoder, HttpRoutes}

import scala.language.higherKinds

class Routes[F[_] : Async](service: Service[F]) extends Http4sDsl[F] with Formats {

  implicit val dateDecoder: EntityDecoder[F, ZonedDateTime] = jsonOf[F, ZonedDateTime]
  implicit val metricsDecoder: EntityDecoder[F, Metrics] = jsonOf[F, Metrics]

  val routes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "health" =>
        Ok(Json.obj())
      case req@POST -> Root / "metrics" / clientId =>
        for {
          metrics <- req.as[Metrics]
          result <- service.addClientMetric(clientId, metrics)
          response <- Ok(result.asJson)
        } yield response
      case GET -> Root / "metrics" / clientId =>
        for {
          result <- service.getClientMetrics(clientId)
          response <- result.fold(NotFound())(body => Ok(body.asJson))
        } yield response
      case GET -> Root / "metrics" =>
        for {
          result <- service.getAllClientsMetrics
          response <- Ok(result.asJson)
        } yield response
      case GET -> Root / "clients" =>
        for {
          result <- service.getClients
          response <- Ok(result.asJson)
        } yield response

    }
}
