package com.hivestreaming.models

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import io.circe.Decoder.Result
import io.circe.{Decoder, Encoder, HCursor, Json}

trait Formats {

  val ZonedDateTimePattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
  val ZonedDateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(ZonedDateTimePattern)

  implicit val ZonedDateTimeFormat: Encoder[ZonedDateTime] with Decoder[ZonedDateTime] = new Encoder[ZonedDateTime] with Decoder[ZonedDateTime] {
    override def apply(t: ZonedDateTime): Json = Encoder.encodeString(t.format(ZonedDateTimeFormatter))

    override def apply(c: HCursor): Result[ZonedDateTime] = Decoder.decodeString.map(s => ZonedDateTime.parse(s, ZonedDateTimeFormatter)).apply(c)
  }

}
