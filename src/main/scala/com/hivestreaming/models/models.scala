package com.hivestreaming

import java.time.ZonedDateTime

package object models {

  case class Metrics(cpu: Double, time: ZonedDateTime)

}