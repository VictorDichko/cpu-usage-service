package com.hivestreaming.store

import cats.effect.Async
import com.hivestreaming.models.Metrics
import scalacache.CatsEffect.asyncForCatsEffectAsync
import scalacache.{Cache, Mode}
import cats.implicits._

import scala.language.higherKinds

class MetricsStore[F[_] : Async](store: Cache[List[Metrics]]) {
  implicit val mode: Mode[F] = new Mode[F] {
    val M: scalacache.Async[F] = asyncForCatsEffectAsync[F]
  }

  def getClientMetrics(clientId: String): F[Option[List[Metrics]]] = {
    store.get(clientId)
  }

  def addClientMetrics(clientId: String, metric: Metrics): F[List[Metrics]] = {
    store.get(clientId).flatMap { metricsOpt =>
      val currentMetrics = metricsOpt.orEmpty
      val updatedMetrics = metric +: currentMetrics
      store.put(clientId)(updatedMetrics).map { _ => updatedMetrics }
    }
  }

}
