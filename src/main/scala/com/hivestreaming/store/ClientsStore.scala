package com.hivestreaming.store

import cats.effect.Async
import cats.implicits._
import scalacache.CatsEffect.asyncForCatsEffectAsync
import scalacache.{Cache, Mode}

import scala.language.higherKinds

class ClientsStore[F[_] : Async](store: Cache[List[String]]) {

  val allClientsKey = "cpu-usage-clients"

  implicit val mode: Mode[F] = new Mode[F] {
    val M: scalacache.Async[F] = asyncForCatsEffectAsync[F]
  }

  def getAll: F[List[String]] = {
    store.get(allClientsKey).map(_.orEmpty)
  }

  def addClient(clientId: String): F[String] = {
    store.get(allClientsKey).flatMap { clientsListOpt =>
      val res: F[_] = if (!clientsListOpt.exists(_.contains(clientId))) {
        store.put(allClientsKey)((clientId +: clientsListOpt.orEmpty).distinct)
      } else {
        Async[F].pure(())
      }
      res.map { _ => clientId }
    }
  }

}
