package com.hivestreaming

import cats.effect.Async
import cats.implicits._
import com.hivestreaming.models.Metrics
import com.hivestreaming.store.{ClientsStore, MetricsStore}

import scala.language.higherKinds

class Service[F[_] : Async](clientsStore: ClientsStore[F], metricsStore: MetricsStore[F]) {

  def addClientMetric(clientId: String, metric: Metrics): F[List[Metrics]] = {
    metricsStore.addClientMetrics(clientId, metric).flatMap { metricsResult =>
      clientsStore.addClient(clientId).map(_ => metricsResult)
    }
  }

  def getClients: F[List[String]] = {
    clientsStore.getAll
  }

  def getClientMetrics(clientId: String): F[Option[List[Metrics]]] = {
    metricsStore.getClientMetrics(clientId)
  }

  def getAllClientsMetrics: F[Map[String, List[Metrics]]] = {
    clientsStore.getAll.flatMap { clients =>
      clients.traverse { clientId =>
        metricsStore.getClientMetrics(clientId).map(_.orEmpty).map(clientId -> _)
      }.map(_.toMap)
    }
  }
}
