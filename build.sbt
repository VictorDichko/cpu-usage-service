name := "cpu-usage-service"

version := "0.1"

scalaVersion := "2.12.6"

val http4sVersion = "0.19.0-SNAPSHOT"
val circeVersion = "0.10.0-M2"
val logbackVersion = "1.2.3"
val scalaCacheVersion = "0.24.3"
val typesafeConfigVersion = "1.3.2"

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-literal" % circeVersion,
  "ch.qos.logback" % "logback-classic" % logbackVersion,
  "com.github.cb372" %% "scalacache-redis" % scalaCacheVersion,
  "com.github.cb372" %% "scalacache-cats-effect" % scalaCacheVersion,
  "com.typesafe" % "config" % typesafeConfigVersion
)

scalacOptions ++= Seq("-Ypartial-unification")

enablePlugins(JavaAppPackaging)